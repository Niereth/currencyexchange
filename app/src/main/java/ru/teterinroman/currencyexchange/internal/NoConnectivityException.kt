package ru.teterinroman.currencyexchange.internal

import java.io.IOException

class NoConnectivityException : IOException() {
}