package ru.teterinroman.currencyexchange.internal

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyEntry
import java.util.Collections.emptyList

class GsonConverter {

    private val gson = Gson()

    @TypeConverter
    fun listToJson(list: List<String>): String {
        return gson.toJson(list)
    }

    @TypeConverter
    fun listFromJson(value: String?): List<String> {
        if (value == null) {
            return emptyList()
        }

        val listType = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(value, listType)
    }

    @TypeConverter
    fun mapToJson(map: Map<String, CurrencyEntry>): String {
        return gson.toJson(map)
    }

    @TypeConverter
    fun mapFromJson(value: String?): Map<String, CurrencyEntry> {
        if (value == null) {
            return emptyMap()
        }

        val mapType = object : TypeToken<Map<String, CurrencyEntry>>() {}.type
        return gson.fromJson(value, mapType)
    }
}