package ru.teterinroman.currencyexchange.data.repository

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.threeten.bp.ZonedDateTime
import ru.teterinroman.currencyexchange.data.db.CurrencyDao
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse
import ru.teterinroman.currencyexchange.data.network.CurrencyNetworkDataSource


class CurrencyRepositoryImpl(
    private val currencyDao: CurrencyDao,
    private val currencyNetworkDataSource: CurrencyNetworkDataSource
) : CurrencyRepository {

    init {
        currencyNetworkDataSource.downloadedCurrency.observeForever { newCurrency ->
            persistFetchedCurrency(newCurrency)
        }
    }

    override suspend fun getActualCurrency(): LiveData<CurrencyResponse> {
        return withContext(Dispatchers.IO) {
            initCurrencyData()
            return@withContext currencyDao.getCurrency()
        }
    }

    private fun persistFetchedCurrency(fetchedCurrency: CurrencyResponse) {
        GlobalScope.launch(Dispatchers.IO) {
            currencyDao.upsert(fetchedCurrency)
        }
    }

    private suspend fun initCurrencyData() {
        // TODO: get Date from response and put in function isFetchCurrencyNeeded()
        if (isFetchCurrencyNeeded(ZonedDateTime.now().minusDays(2))) {
            fetchCurrency()
        }
    }

    private suspend fun fetchCurrency() {
        currencyNetworkDataSource.fetchCurrency()
    }

    private fun isFetchCurrencyNeeded(lastFetchTime: ZonedDateTime): Boolean {
        val oneDayAgo = ZonedDateTime.now().minusDays(1)
        return lastFetchTime.isBefore(oneDayAgo)
    }
}