package ru.teterinroman.currencyexchange.data.network

import androidx.lifecycle.LiveData
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse


interface CurrencyNetworkDataSource {

    val downloadedCurrency: LiveData<CurrencyResponse>

    suspend fun fetchCurrency()
}