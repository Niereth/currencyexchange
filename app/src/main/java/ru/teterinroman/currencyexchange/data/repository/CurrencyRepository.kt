package ru.teterinroman.currencyexchange.data.repository

import androidx.lifecycle.LiveData
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse

interface CurrencyRepository {

    suspend fun getActualCurrency(): LiveData<CurrencyResponse>
}
