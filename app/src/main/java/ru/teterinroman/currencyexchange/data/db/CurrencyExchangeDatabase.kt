package ru.teterinroman.currencyexchange.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse
import ru.teterinroman.currencyexchange.internal.GsonConverter


@Database(
    entities = [CurrencyResponse::class],
    version = 1
)
@TypeConverters(value = [GsonConverter::class])
abstract class CurrencyExchangeDatabase : RoomDatabase() {

    abstract fun currencyDao(): CurrencyDao

    companion object {
        @Volatile
        private var instance: CurrencyExchangeDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context) = instance ?: synchronized(LOCK) {
            instance ?: buildDatabase(context).also { instance = it }
        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                CurrencyExchangeDatabase::class.java,
                "currency_exchange.db"
            ).build()
    }
}