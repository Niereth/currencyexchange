package ru.teterinroman.currencyexchange.data.db

//import ru.teterinroman.currencyexchange.data.db.entity.CURRENCY_ID
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse


@Dao
interface CurrencyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsert(currenciesEntries: CurrencyResponse)

    @Query("SELECT * FROM currency")
    fun getCurrency(): LiveData<CurrencyResponse>
}