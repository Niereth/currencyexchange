package ru.teterinroman.currencyexchange.data.network

import android.util.Log
import androidx.lifecycle.MutableLiveData
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse
import ru.teterinroman.currencyexchange.internal.NoConnectivityException


class CurrencyNetworkDataSourceImpl(
    private val currencyApiService: CurrencyApiService
) : CurrencyNetworkDataSource {

    override val downloadedCurrency: MutableLiveData<CurrencyResponse> = MutableLiveData()

    override suspend fun fetchCurrency() {
        try {
            val fetchedCurrency = currencyApiService
                .getCurrencyAsync()
                .await()
            downloadedCurrency.postValue(fetchedCurrency)
        } catch (e: NoConnectivityException) {
            Log.e("Connectivity", "No internet connection", e)
        }
    }
}