package ru.teterinroman.currencyexchange.data.db.entity

import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


data class CurrencyEntry(
    @PrimaryKey(autoGenerate = false)
    @SerializedName("ID")
    val id: String,
    @SerializedName("NumCode")
    val numCode: String,
    @SerializedName("CharCode")
    val charCode: String,
    @SerializedName("Nominal")
    val nominal: Int,
    @SerializedName("Name")
    val name: String,
    @SerializedName("Value")
    val value: Double,
    @SerializedName("Previous")
    val previous: Double,
    var isFavorite: Boolean
)