package ru.teterinroman.currencyexchange.data.db.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.SerializedName
import ru.teterinroman.currencyexchange.internal.GsonConverter

const val CURRENCY_ID = 0


@Entity(tableName = "currency")
data class CurrencyResponse(
    @SerializedName("Date")
    val date: String,
    @SerializedName("PreviousDate")
    val previousDate: String,
    @SerializedName("PreviousURL")
    val previousURL: String,
    @SerializedName("Timestamp")
    val timestamp: String,
    @SerializedName("Valute")
    @TypeConverters(GsonConverter::class)
    val currencyEntry: Map<String, CurrencyEntry>
) {
    @PrimaryKey(autoGenerate = false)
    var id: Int = CURRENCY_ID
}