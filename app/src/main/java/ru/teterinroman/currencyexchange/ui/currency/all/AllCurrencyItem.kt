package ru.teterinroman.currencyexchange.ui.currency.all

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_currency_all.*
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyEntry

class AllCurrencyItem(
    private val currencyEntry: CurrencyEntry,
    private val context: Context
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            updateCurrency()
        }
    }

    override fun getLayout() = R.layout.item_currency_all

    private fun ViewHolder.updateCurrency() {
        textView_charCode.text = currencyEntry.charCode
        textView_info.text = String.format(
            context.getString(R.string.currency_info_equals),
            currencyEntry.nominal,
            currencyEntry.name
        )
        textView_value.text = String.format(
            context.getString(R.string.currency_value_rub),
            currencyEntry.value
        )
    }
}