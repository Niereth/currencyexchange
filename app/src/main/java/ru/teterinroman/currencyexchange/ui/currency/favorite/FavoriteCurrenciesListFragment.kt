package ru.teterinroman.currencyexchange.ui.currency.favorite

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.network.ConnectivityInterceptorImpl
import ru.teterinroman.currencyexchange.data.network.CurrencyApiService
import ru.teterinroman.currencyexchange.data.network.CurrencyNetworkDataSourceImpl

class FavoriteCurrenciesListFragment : Fragment() {

    companion object {
        fun newInstance() = FavoriteCurrenciesListFragment()
    }

    private lateinit var viewModel: FavoriteCurrenciesListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.favorite_currencies_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(FavoriteCurrenciesListViewModel::class.java)
        // TODO: Use the ViewModel

        val apiService = CurrencyApiService(ConnectivityInterceptorImpl(this.requireContext()))
        val currencyNetworkDataSource = CurrencyNetworkDataSourceImpl(apiService)

        currencyNetworkDataSource.downloadedCurrency.observe(viewLifecycleOwner, Observer {
//            textView.text = it.toString()
        })

        GlobalScope.launch(Dispatchers.Main) {
            currencyNetworkDataSource.fetchCurrency()
        }
    }

}