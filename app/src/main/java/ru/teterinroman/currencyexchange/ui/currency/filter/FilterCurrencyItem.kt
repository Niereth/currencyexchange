package ru.teterinroman.currencyexchange.ui.currency.filter

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_currency_filter.*
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyEntry


class FilterCurrencyItem(
    private val currencyEntry: CurrencyEntry,
    private val context: Context
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            updateCurrency()
        }
    }

    override fun getLayout() = R.layout.item_currency_filter

    private fun ViewHolder.updateCurrency() {

        textView_radio_button.setOnClickListener {
            currencyEntry.isFavorite = !currencyEntry.isFavorite
        }

        textView_charCode.text = currencyEntry.charCode
        textView_info.text = String.format(
            context.getString(R.string.currency_info),
            currencyEntry.nominal,
            currencyEntry.name
        )
    }
}