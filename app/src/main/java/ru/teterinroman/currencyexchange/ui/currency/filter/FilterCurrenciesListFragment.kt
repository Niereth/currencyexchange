package ru.teterinroman.currencyexchange.ui.currency.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.filter_currencies_list_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse
import ru.teterinroman.currencyexchange.ui.base.CurrenciesListViewModel
import ru.teterinroman.currencyexchange.ui.base.CurrenciesListViewModelFactory
import ru.teterinroman.currencyexchange.ui.base.ScopedFragment

class FilterCurrenciesListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: CurrenciesListViewModelFactory by instance()
    private lateinit var viewModel: CurrenciesListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.filter_currencies_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(CurrenciesListViewModel::class.java)

        bindUI()
    }

    private fun bindUI() = launch {
        val currency = viewModel.currency.await()
        currency.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) return@Observer

            group_loading.visibility = View.GONE
            updateDate(response.date)
            initRecyclerView(response.toCurrencyItems())
        })
    }

    private fun updateDate(date: String) {
        (activity as? AppCompatActivity)?.supportActionBar?.title = date
    }

    private fun CurrencyResponse.toCurrencyItems(): List<FilterCurrencyItem> {
        return this.currencyEntry.entries.map {
            FilterCurrencyItem(it.value, requireContext())
        }
    }

    private fun initRecyclerView(items: List<FilterCurrencyItem>) {
        val groupAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(items)
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@FilterCurrenciesListFragment.context)
            adapter = groupAdapter
        }
    }

}