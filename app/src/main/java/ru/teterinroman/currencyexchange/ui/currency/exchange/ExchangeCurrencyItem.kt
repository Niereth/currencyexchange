package ru.teterinroman.currencyexchange.ui.currency.exchange

import android.content.Context
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_currency_exchange.*
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyEntry

class ExchangeCurrencyItem(
    private val currencyEntry: CurrencyEntry,
    private val baseValue: Double?,
    private val context: Context
) : Item() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.apply {
            updateCurrency()
        }
    }

    override fun getLayout() = R.layout.item_currency_exchange

    private fun ViewHolder.updateCurrency() {
        textView_charCode.text = currencyEntry.charCode
        textView_name.text = currencyEntry.name

        if (baseValue != null) {
            textView_converted_value.text = String.format(
                context.getString(R.string.currency_value),
                baseValue * currencyEntry.nominal / currencyEntry.value
            )
        }
    }
}