package ru.teterinroman.currencyexchange.ui.currency.exchange

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.exchange_currencies_list_fragment.*
import kotlinx.coroutines.launch
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.closestKodein
import org.kodein.di.generic.instance
import ru.teterinroman.currencyexchange.R
import ru.teterinroman.currencyexchange.data.db.entity.CurrencyResponse
import ru.teterinroman.currencyexchange.ui.base.CurrenciesListViewModel
import ru.teterinroman.currencyexchange.ui.base.CurrenciesListViewModelFactory
import ru.teterinroman.currencyexchange.ui.base.ScopedFragment


class ExchangeCurrenciesListFragment : ScopedFragment(), KodeinAware {

    override val kodein by closestKodein()
    private val viewModelFactory: CurrenciesListViewModelFactory by instance()
    private lateinit var viewModel: CurrenciesListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.exchange_currencies_list_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProvider(this, viewModelFactory)
            .get(CurrenciesListViewModel::class.java)

        bindUI()

        button_convert.setOnClickListener {
            bindUI()
        }
    }

    private fun bindUI() = launch {
        val currency = viewModel.currency.await()
        currency.observe(viewLifecycleOwner, Observer { response ->
            if (response == null) return@Observer

            group_loading.visibility = View.GONE
            updateDate(response.date)
            initRecyclerView(response.toCurrencyItems())
        })
    }

    private fun updateDate(date: String) {
        (activity as? AppCompatActivity)?.supportActionBar?.title = date
    }

    private fun CurrencyResponse.toCurrencyItems(): List<ExchangeCurrencyItem> {
        return this.currencyEntry.entries.map {
            val parsedRusValue = edit_text_rus_value.text.toString().toDoubleOrNull()
            ExchangeCurrencyItem(it.value, parsedRusValue, requireContext())
        }
    }

    private fun initRecyclerView(items: List<ExchangeCurrencyItem>) {
        val groupAdapter = GroupAdapter<ViewHolder>().apply {

            // add in List only Favorite. But always False (((
//            for (item in items) {
//                if (item.currencyEntry.isFavorite) {
//                    add(item)
//                }
//            }

            addAll(items)
        }

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@ExchangeCurrenciesListFragment.context)
            adapter = groupAdapter
        }
    }

}