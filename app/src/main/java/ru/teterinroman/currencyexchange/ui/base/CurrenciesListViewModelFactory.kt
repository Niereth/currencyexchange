package ru.teterinroman.currencyexchange.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.teterinroman.currencyexchange.data.repository.CurrencyRepository


class CurrenciesListViewModelFactory(
    private val currencyRepository: CurrencyRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CurrenciesListViewModel(currencyRepository) as T
    }
}