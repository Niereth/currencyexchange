package ru.teterinroman.currencyexchange.ui.base

import androidx.lifecycle.ViewModel
import ru.teterinroman.currencyexchange.data.repository.CurrencyRepository
import ru.teterinroman.currencyexchange.internal.lazyDeferred


class CurrenciesListViewModel(
    private val currencyRepository: CurrencyRepository
) : ViewModel() {

    val currency by lazyDeferred {
        currencyRepository.getActualCurrency()
    }
}