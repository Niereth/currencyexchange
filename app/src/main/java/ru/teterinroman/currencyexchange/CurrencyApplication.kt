package ru.teterinroman.currencyexchange

import android.app.Application
import com.jakewharton.threetenabp.AndroidThreeTen
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton
import ru.teterinroman.currencyexchange.data.db.CurrencyExchangeDatabase
import ru.teterinroman.currencyexchange.data.network.*
import ru.teterinroman.currencyexchange.data.repository.CurrencyRepository
import ru.teterinroman.currencyexchange.data.repository.CurrencyRepositoryImpl
import ru.teterinroman.currencyexchange.ui.base.CurrenciesListViewModelFactory

class CurrencyApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@CurrencyApplication))

        bind() from singleton { CurrencyExchangeDatabase(instance()) }
        bind() from singleton { instance<CurrencyExchangeDatabase>().currencyDao() }
        bind<ConnectivityInterceptor>() with singleton { ConnectivityInterceptorImpl(instance()) }
        bind() from singleton { CurrencyApiService(instance()) }
        bind<CurrencyNetworkDataSource>() with singleton { CurrencyNetworkDataSourceImpl(instance()) }
        bind<CurrencyRepository>() with singleton { CurrencyRepositoryImpl(instance(), instance()) }
        bind() from provider { CurrenciesListViewModelFactory(instance()) }
    }

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
    }
}